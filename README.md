## Aprendiendo a analizar código en Ruby

Para este ejercicio hay que documentarse un poco acerca de operaciones aritméticas en ruby.

Analiza el siguiente código y `OJO` sin ejecutar el programa coloca el resultado que corresponda de la comparación.

```Ruby
suma_uno = 8 + 10
suma_dos = suma_uno + 10
valor_uno = 10
suma_dos == 18 + valor_uno
#=> Escribe el resultado de la comparación

resta_uno = 54 - 10
valor_uno = "cuarenta"
valor_dos = "ocho"
suma_uno = valor_uno + valor_dos
resta_dos = resta_uno - suma_uno
resta_dos == 0
#=> Escribe el resultado de la comparación

multiplicacion = 10 * 5
division = multiplicacion / 5
valor_uno = 8
operacion = division - valor_uno
valor_dos = "World"
cadena = "Hello" * operacion +  valor_dos
cadena == "HelloHelloHelloWorld"
#=> Escribe el resultado de la comparación

```	